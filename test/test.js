const assert = require('assert');
const findSumOfMultiples = require('../index.js');


describe('Problem1', function() {
  it('sum of multiples of 3 or 5 is correct for 10', function() {
    assert.equal(findSumOfMultiples(10), 23);
  });
  it('sum of multiples of 3 or 5 is correct for 1000', function() {
    assert.equal(findSumOfMultiples(1000), 233168);
  });
});